## Data cleaning

### Instructions

You need g++ to be installed in your system.

Run `make` to compile the program.

In order to generate the cleaned data, use:

```bash
mkdir out
./data-cleaner data/BxM.txt data/BxM_2.txt out/cleaned_data.txt out/value_translations.json
```