import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UploadGuard} from './guards';
import {CantFindComponent} from './components';


const routes: Routes = [
	{
		path: '',
		redirectTo: 'visualizer',
		pathMatch: 'full'
	},
	{
		path: 'upload',
		loadChildren: () => import('./modules/upload/upload.module').then(m => m.UploadModule),
		canLoad: [UploadGuard],
	},
	{
		path: 'visualizer',
		loadChildren: () => import('./modules/visualizer/visualizer.module').then(m => m.VisualizerModule),
		canLoad: [UploadGuard],
	},
	{
		path: '**',
		component: CantFindComponent,
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
