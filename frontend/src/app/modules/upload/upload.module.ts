import { NgModule } from '@angular/core';
import { UploadComponent } from './pages';
import { UploadService } from './services/upload.service';
import { SharedModule } from '../shared/shared.module';
import { UploadRoutingModule } from './upload-routing.module';
import { UploadCardComponent } from './component';


@NgModule({
	declarations: [
		UploadComponent,
		UploadCardComponent
	],
	imports: [
		SharedModule,
		UploadRoutingModule
	],
	providers: [UploadService],
	bootstrap: [UploadComponent]
})
export class UploadModule {}
