import {Component, Input} from '@angular/core';
import {UploadCard, UploadCardType} from '../../interfaces';

@Component({
	selector: 'app-upload-card',
	templateUrl: './upload-card.component.html',
	styleUrls: ['./upload-card.component.css']
})
export class UploadCardComponent {

	@Input()
	public card: UploadCard;

	constructor() {}

	public getIcon(): string {
		return `assets/uploaded-cards-icons/${this.card.type}.png`;
	}

	public getTitle(): string {
		return this.card.sizeInBytes.toString() + ' Bytes';
	}

	public getDate(): string {
		return this.card.type === UploadCardType.SAMPLE ? 'Sample' : '09/12/2019';
	}
}
