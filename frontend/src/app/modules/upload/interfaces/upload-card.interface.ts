export enum UploadCardType {
	SAMPLE = 'sample',
	UPLOADED = 'uploaded',
}

export interface UploadCard {
	type: UploadCardType;
	title: string;
	sizeInBytes: number;
	date: Date;
}
