import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {UploadService} from '../../services/upload.service';
import {AppService} from '../../../../services';
import {Observable} from 'rxjs';
import {UploadCard} from '../../interfaces';

@Component({
	selector: 'app-upload',
	templateUrl: './upload.component.html',
	styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
	@ViewChild('basicResults', {static: false})
	private basicResults: ElementRef<HTMLInputElement>;

	@ViewChild('geneticResults', {static: false})
	private geneticResults: ElementRef<HTMLInputElement>;

	public basicResultsUploaded$: Observable<boolean>;
	public geneticResultsUploaded$: Observable<boolean>;

	public sampleCards$: Observable<UploadCard[]>;
	public uploadedCards$: Observable<UploadCard[]>;

	public sampleCardsSelected$: Observable<boolean>;
	public uploadedCardsSelected$: Observable<boolean>;

	private moduleDescription: string[] = [
		'Upload your dataset here!',
		'The mosquito will drain its data so you can play with it.'
	];

	constructor(
		private service: UploadService,
		private appService: AppService,
		private renderer: Renderer2
	) {
	}

	ngOnInit() {
		setTimeout(() => {
			this.appService.setDescription(this.moduleDescription);
		}, 50);

		this.basicResultsUploaded$ = this.service.basicResults$;
		this.geneticResultsUploaded$ = this.service.geneticResults$;

		this.sampleCards$ = this.service.sampleCards$;
		this.uploadedCards$ = this.service.uploadedCards$;

		this.sampleCardsSelected$ = this.service.sampleCardsSelected$;
		this.uploadedCardsSelected$ = this.service.uploadedCardsSelected$;
	}

	public clickOnBasicResults(event: MouseEvent): void {
		this.basicResults.nativeElement.value = null;
		this.renderer.selectRootElement(this.basicResults.nativeElement).click();
	}

	public clickOnGeneticResults(event: MouseEvent): void {
		this.geneticResults.nativeElement.value = null;
		this.renderer.selectRootElement(this.geneticResults.nativeElement).click();
	}

	public somethingUploaded(): boolean {
		return this.service.somethingUploaded();
	}

	public selectSampleCards(): void {
		this.service.selectSampleCards();
	}

	public selectUploadedCards(): void {
		this.service.selectUploadedCards();
	}

	public canContinue(): boolean {
		return this.service.canContinue();
	}

	public uploadResults(event: Event, basic: boolean): void {
		if ((event.target as HTMLInputElement).files && (event.target as HTMLInputElement).files.length) {
			const reader = new FileReader();

			const file = (event.target as HTMLInputElement).files[0];

			this.service.uploadResult(file.name, file, basic);
		}
	}
}
