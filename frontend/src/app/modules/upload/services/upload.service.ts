import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UploadCard, UploadCardType} from '../interfaces';
import {ApiService} from '../../../services';

@Injectable()
export class UploadService {

	private basicResultsStart = false;
	private basicResults: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.basicResultsStart);

	private geneticResultsStart = false;
	private geneticResults: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.geneticResultsStart);

	private sampleCardsStart: UploadCard[] = [
		{date: new Date(), sizeInBytes: 620, title: 'BxM Basic Results', type: UploadCardType.SAMPLE},
		{date: new Date(), sizeInBytes: 620, title: 'BxM Genetic Results', type: UploadCardType.SAMPLE},
	];
	private sampleCards: BehaviorSubject<UploadCard[]> = new BehaviorSubject<UploadCard[]>(this.sampleCardsStart);

	private uploadedCardsStart: UploadCard[] = [
		{date: new Date(), sizeInBytes: 620, title: 'BxM Basic Results', type: UploadCardType.UPLOADED},
		{date: new Date(), sizeInBytes: 620, title: 'BxM Basic Results', type: UploadCardType.UPLOADED},
	];
	private uploadedCards: BehaviorSubject<UploadCard[]> = new BehaviorSubject<UploadCard[]>(this.uploadedCardsStart);

	private sampleCardsSelectedStart = false;
	private sampleCardsSelected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.sampleCardsSelectedStart);

	private uploadedCardsSelectedStart = false;
	private uploadedCardsSelected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.uploadedCardsSelectedStart);

	public basicResults$: Observable<boolean> = this.basicResults;
	public geneticResults$: Observable<boolean> = this.geneticResults;

	public sampleCards$: Observable<UploadCard[]> = this.sampleCards.asObservable();
	public uploadedCards$: Observable<UploadCard[]> = this.uploadedCards.asObservable();

	public sampleCardsSelected$: Observable<boolean> = this.sampleCardsSelected.asObservable();
	public uploadedCardsSelected$: Observable<boolean> = this.uploadedCardsSelected.asObservable();

	constructor(private api: ApiService) {
	}

	public somethingUploaded(): boolean {
		return this.uploadedCards.value.length > 0;
	}

	public selectSampleCards(): void {
		this.sampleCardsSelected.next(!this.sampleCardsSelected.value);
		this.uploadedCardsSelected.next(false);

		console.log({sample: this.sampleCardsSelected.value, uploaded: this.uploadedCardsSelected.value});
	}

	public selectUploadedCards(): void {
		this.sampleCardsSelected.next(false);
		this.uploadedCardsSelected.next(!this.uploadedCardsSelected.value);
	}

	public canContinue(): boolean {
		return this.sampleCardsSelected.value || this.uploadedCardsSelected.value;
	}

	public uploadResult(fileName: string, file: File, basic: boolean): void {

		const formData: FormData = new FormData();
		formData.append('file', file, fileName);

		if (basic) {
			this.api.uploadResults(formData, basic);
		}

	}
}
