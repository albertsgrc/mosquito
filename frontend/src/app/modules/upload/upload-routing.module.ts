import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UploadComponent} from './pages';


const routes: Routes = [
	{
		path: '',
		component: UploadComponent
	},
	{
		path: '**',
		redirectTo: '',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UploadRoutingModule {
}
