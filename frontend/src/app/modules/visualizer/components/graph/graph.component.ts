import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {GraphService} from '../../services';
import {LibraryGraph, VisualizerData} from '../../interfaces';

@Component({
	selector: 'app-graph',
	templateUrl: './graph.component.html',
	styleUrls: ['./graph.component.css']
})
export class GraphComponent implements AfterViewInit {

	@ViewChild('canvas', {static: false})
	private canvas: ElementRef<HTMLDivElement>;

	private graph: LibraryGraph;

	@Input()
	private data: VisualizerData = {nodes: [], links: []};

	constructor(private service: GraphService) {
	}

	ngAfterViewInit() {
		this.configureGraph();
	}

	private configureGraph(): void {
		this.graph = this.service.generateGraph(this.canvas.nativeElement);

		this.onWindowResize();
	}

	public onWindowResize(): void {
		const {clientWidth, clientHeight} = this.canvas.nativeElement;

		this.service.onWindowResize(clientWidth, clientHeight);
	}
}
