import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VisualizerComponent} from './pages';


const routes: Routes = [
	{
		path: '',
		component: VisualizerComponent
	},
	{
		path: '**',
		redirectTo: '',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class VisualizerRoutingModule {
}
