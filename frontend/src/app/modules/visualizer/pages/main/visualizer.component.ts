import {Component, OnInit} from '@angular/core';
import {VisualizerService} from '../../services';
import {ApiService, AppService} from '../../../../services';
import {VisualizerData} from '../../interfaces';
import {Observable} from 'rxjs';

@Component({
	selector: 'app-visualizer',
	templateUrl: './visualizer.component.html',
	styleUrls: ['./visualizer.component.css']
})
export class VisualizerComponent implements OnInit {

	private moduleDescription: string[] = [
		'This is the visualizer',
		'You can see the cluster by yourself.'
	];

	constructor(private service: VisualizerService, private app: AppService, private api: ApiService) {
	}

	ngOnInit() {
		setTimeout(() => {
			this.app.setDescription([...this.moduleDescription]);
		});
	}
}
