import {
	Mesh,
	PerspectiveCamera,
	Scene,
	WebGLRenderer,
} from 'three';

export interface XYZposition {
	x: number;
	y: number;
	z: number;
}

export interface VisualizerData {
	nodes: VisualizerNode[];
	links: VisualizerLink[];
}

export interface VisualizerNode {
	id: string;
	label: string;
	group: NodeType;

	__threeObj?: Mesh; // added by the 3d-force-graph library
	x?: number; // added by the 3d-force-graph library
	y?: number; // added by the 3d-force-graph library
	z?: number; // added by the 3d-force-graph library
	vx?: number; // added by the 3d-force-graph library
	vy?: number; // added by the 3d-force-graph library
	vz?: number; // added by the 3d-force-graph library
}

export interface VisualizerLink {
	id: string;

	source: VisualizerNode;
	target: VisualizerNode;

	force: number;

	__lineObj?: Mesh; // added by the 3d-force-graph library
	x?: number; // added by the 3d-force-graph library
	y?: number; // added by the 3d-force-graph library
	z?: number; // added by the 3d-force-graph library
}

// This is a mock-up interface so the tslint doesn't get mad
export interface LibraryGraph {
	backgroundColor(): string;

	backgroundColor(bgColor: string): LibraryGraph;

	camera(): PerspectiveCamera;

	cameraPosition(): XYZposition;

	cameraPosition(position: XYZposition, lookAt: XYZposition, ms: number): void;

	controls(): {
		addEventListener: (eventName: string, cb: () => void) => void;
		dispose: () => void;
		handleResize: () => void;
		keys: [number, number, number];
		noRotate: boolean;
	};

	cooldownTicks(): number;

	cooldownTicks(ticks: number): LibraryGraph;

	cooldownTime(): number;

	cooldownTime(time: number): LibraryGraph;

	d3AlphaDecay(): number;

	d3AlphaDecay(decay: number): LibraryGraph;

	d3Force(
		force: 'link' | 'center' | 'charge' | 'radial'
	): {
		distance(dist: ((link: VisualizerLink) => number) | number);
		strength(dist: ((link: VisualizerLink) => number) | number);
	};

	d3ReheatSimulation(): LibraryGraph;

	d3VelocityDecay(): number;

	d3VelocityDecay(decay: number): LibraryGraph;

	enableNodeDrag(): boolean;

	enableNodeDrag(enable: boolean): LibraryGraph;

	graphData(): VisualizerData;

	graphData(data: VisualizerData): LibraryGraph;

	height(): number;

	height(height: number): LibraryGraph;

	linkColor(colorFn: (link: VisualizerLink) => string): LibraryGraph;

	linkOpacity(): number;

	linkOpacity(opacity: number): LibraryGraph;

	linkResolution(): number;

	linkResolution(resolution: number): LibraryGraph;

	linkThreeObject(meshFn: (link: VisualizerLink) => Mesh): LibraryGraph;

	linkVisibility(visFn: (link: VisualizerLink) => boolean): LibraryGraph;

	linkWidth(widthFn: (link: VisualizerLink) => number): LibraryGraph;

	nodeColor(colorFn: (node: VisualizerNode) => string): LibraryGraph;

	nodeOpacity(): number;

	nodeOpacity(opacity: number): LibraryGraph;

	nodeResolution(): number;

	nodeResolution(resolution: number): LibraryGraph;

	nodeThreeObject(meshFn: (node: VisualizerNode) => Mesh): LibraryGraph;

	nodeLabel(label: string): LibraryGraph;

	nodeVal(sizeFn: (node: VisualizerNode) => number): LibraryGraph;

	nodeVisibility(visFn: (node: VisualizerNode) => boolean): LibraryGraph;

	numDimensions(): number;

	numDimensions(dim: number): LibraryGraph;

	onBackgroundClick(handler: (event: MouseEvent) => void): LibraryGraph;

	onBackgroundRightClick(handler: (event: MouseEvent) => void): LibraryGraph;

	onEngineTick(fn: () => void): LibraryGraph;

	onEngineStop(fn: () => void): LibraryGraph;

	onNodeClick(handler: (node: VisualizerNode, event: MouseEvent) => void): LibraryGraph;

	onNodeHover(handler: (node: VisualizerNode, event: MouseEvent) => void): LibraryGraph;

	onNodeRightClick(
		handler: (node: VisualizerNode, event: MouseEvent) => void
	): LibraryGraph;

	pauseAnimation(): LibraryGraph;

	refresh();

	renderer(): WebGLRenderer;

	scene(): Scene;

	showNavInfo(): boolean;

	showNavInfo(show: boolean): LibraryGraph;

	warmupTicks(): number;

	warmupTicks(ticks: number): LibraryGraph;

	width(): number;

	width(width: number): LibraryGraph;
}

export enum NodeType {
	SUBJECT = 'Subject',
	MUTATION = 'Mutation',
	GEN = 'Gen'
}
