export type GraphPage = GraphRow[];

export interface GraphRow {
	subject: string;
	mutation: string;
	gen: string;
}
