import {Injectable} from '@angular/core';
import {ApiService} from '../../../services';
import {GraphPage, NodeType, VisualizerData, VisualizerLink, VisualizerNode} from '../interfaces';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class VisualizerService {

	private subjectMap: { [name: string]: VisualizerNode } = {};
	private genMap: { [name: string]: VisualizerNode } = {};
	private mutationMap: { [name: string]: VisualizerNode } = {};

	private subjectToMutation: { [subject: string]: string[] } = {};
	private mutationToGen: { [mutation: string]: string[] } = {};

	private nodes: VisualizerNode[] = [];
	private links: VisualizerLink[] = [];

	private graphDataStart: VisualizerData = {nodes: [], links: []};
	private graphData: BehaviorSubject<VisualizerData> = new BehaviorSubject<VisualizerData>(this.graphDataStart);

	public graphData$: Observable<VisualizerData> = this.graphData.asObservable();

	private nextPage = 0;
	private limit = 100;
	private errors = 0;
	private maxErrors = 3;

	constructor(private api: ApiService) {
	}

	public getData(): void {
		this.api.getGraphPage(this.limit, this.nextPage)
			.then((page) => {
				this.nextPage += this.limit;

				if (page.length > 0) {

					this.parsePage(page);

					setTimeout(() => {
						this.getData();
					}, 50);

				}

			})
			.catch((err) => {
				console.error(err);
				this.errors++;
				if (this.errors < this.maxErrors) {
					setTimeout(() => {
						this.getData();
					}, 100);
				} else {
					console.error('We could retrieve the next page :(');
				}
			});
	}

	private parsePage(page: GraphPage): void {
		for (const row of page) {
			const {subject, gen, mutation} = row;

			const subjectNode = this.subjectMap[subject] ? this.subjectMap[subject] : this.createNode(subject, NodeType.SUBJECT);
			const genNode = this.genMap[gen] ? this.genMap[gen] : this.createNode(gen, NodeType.GEN);
			const mutationNode = this.mutationMap[mutation] ? this.mutationMap[mutation] : this.createNode(mutation, NodeType.MUTATION);

			this.createLinks(subjectNode, genNode, mutationNode);
		}

		this.graphData.next({
			nodes: this.nodes,
			links: this.links,
		});
	}

	private createNode(name: string, type: NodeType): VisualizerNode {
		const node = {
			group: type,
			id: name,
			label: type + ': ' + name,
		};

		if (type === NodeType.SUBJECT) {
			this.subjectMap[name] = node;
		} else if (type === NodeType.MUTATION) {
			this.mutationMap[name] = node;
		} else {
			this.genMap[name] = node;
		}

		this.nodes = [...this.nodes, node];

		return node;
	}

	private createLinks(subject: VisualizerNode, gen: VisualizerNode, mutation: VisualizerNode) {
		if (!this.subjectToMutation[subject.label]) {
			this.subjectToMutation[subject.label] = [];
		}

		if (!this.mutationToGen[mutation.label]) {
			this.mutationToGen[mutation.label] = [];
		}

		if (!this.subjectToMutation[subject.label].includes(mutation.label)) {
			this.subjectToMutation[subject.label].push(mutation.label);

			const edge: VisualizerLink = {
				id: 'SUBJECT' + subject.label + 'MUTATION' + mutation.label,
				force: 1,
				source: subject,
				target: mutation,
			};

			this.links = [...this.links, edge];
		}

		if (!this.mutationToGen[mutation.label].includes(gen.label)) {
			this.mutationToGen[mutation.label].push(gen.label);

			const edge: VisualizerLink = {
				id: 'MUTATION' + mutation.label + 'GEN' + gen.label,
				force: 1,
				source: mutation,
				target: gen,
			};

			this.links = [...this.links, edge];
		}
	}
}
