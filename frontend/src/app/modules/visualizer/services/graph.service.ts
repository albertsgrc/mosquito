import {Injectable} from '@angular/core';
import {LibraryGraph, NodeType, VisualizerData, VisualizerLink, VisualizerNode} from '../interfaces';

import ForceGraph3D from '3d-force-graph';
import {VisualizerService} from './visualizer.service';

@Injectable()
export class GraphService {

	private graph: LibraryGraph;

	private static arrayToString = (rgba: number[]) => {
		return `rgba(${rgba.join(', ')})`;
	}

	constructor(private visualizer: VisualizerService) {
	}

	public generateGraph(canvas: HTMLDivElement): LibraryGraph {

		const rendererConfig: { antialiasing: boolean; alpha: boolean } = {
			antialiasing: true,
			alpha: true,
		};
		this.graph = ForceGraph3D(rendererConfig)(canvas);
		this.graph.numDimensions(3);
		this.graph.showNavInfo(true);
		this.graph.warmupTicks(0);
		this.graph.cooldownTicks(Infinity);
		this.graph.cooldownTime(7000);
		this.graph.backgroundColor('#00000000');
		this.graph.enableNodeDrag(false);
		this.graph.nodeVisibility(this.nodeVisibility.bind(this));
		this.graph.linkVisibility(this.linkVisibility.bind(this));
		this.graph.nodeOpacity(1);
		this.graph.linkOpacity(1);
		this.graph.nodeColor(this.nodeColor.bind(this));
		this.graph.nodeResolution();
		this.graph.linkColor(this.linkColor.bind(this));
		this.graph.nodeVal(this.nodeSize.bind(this));
		this.graph.linkWidth(this.linkWidth.bind(this));
		this.graph.renderer().setPixelRatio(window.devicePixelRatio);
		this.graph.d3Force('charge').strength(-200);
		this.graph.d3Force('link').strength(this.linkStrength.bind(this));
		this.graph.d3Force('link').distance(this.linkDistance.bind(this));
		this.graph.d3AlphaDecay(0.006);
		this.graph.d3VelocityDecay(0.2);
		this.graph.onEngineTick(this.onEngineTick.bind(this));
		this.graph.onEngineStop(this.onEngineStop.bind(this));
		this.graph.onNodeHover(this.handleHover.bind(this));
		this.graph.nodeLabel('label');
		// this.graph.onNodeClick(this.handleClick.bind(this));

		// this.graph
		// 	.controls()
		// 	.addEventListener('change', this.onChangeOnControls.bind(this));

		this.visualizer.graphData$.subscribe((data) => {
			this.graph.graphData(data);
		});

		this.visualizer.getData();

		return this.graph;
	}

	public onWindowResize(clientWidth: number, clientHeight: number): void {
		if (this.graph) {
			this.graph.width(clientWidth);
			this.graph.height(clientHeight);
			this.graph.controls().handleResize();
		}
	}

	private nodeColor(node: VisualizerNode): string {
		const typeToColor: { [type in NodeType]: [number, number, number] } = {
			[NodeType.SUBJECT]: [55, 160, 240],
			[NodeType.MUTATION]: [55, 240, 160],
			[NodeType.GEN]: [240, 55, 160],
		};

		return GraphService.arrayToString([...typeToColor[node.group], this.nodeOpacity(node)]);
	}

	private nodeOpacity(node: VisualizerNode): number {
		return 0.8;
	}

	private linkColor(link: VisualizerLink): string {
		return GraphService.arrayToString([55, 160, 240, 0.3]);
	}

	private nodeVisibility(node: VisualizerNode): boolean {
		return true;
	}

	private linkVisibility(link: VisualizerLink): boolean {
		return true;
	}

	private nodeSize(node: VisualizerNode): number {
		return 10;
	}

	private linkWidth(link: VisualizerLink): number {
		return 1;
	}

	private linkStrength(link: VisualizerLink): number {
		return 1;
	}

	private linkDistance(link: VisualizerLink): number {
		return 20;
	}

	private onEngineTick(): void {

	}

	private onEngineStop(): void {

	}

	private handleHover(node: VisualizerNode): void {

	}

	private handleClick(node: VisualizerNode): void {

	}
}
