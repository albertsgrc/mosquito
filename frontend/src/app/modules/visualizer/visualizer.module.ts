import {NgModule} from '@angular/core';
import {VisualizerComponent} from './pages';
import {GraphService, VisualizerService} from './services';
import {SharedModule} from '../shared/shared.module';
import {VisualizerRoutingModule} from './visualizer-routing.module';
import {GraphComponent} from './components';


@NgModule({
	declarations: [
		VisualizerComponent,
		GraphComponent,
	],
	imports: [
		SharedModule,
		VisualizerRoutingModule
	],
	providers: [VisualizerService, GraphService],
	bootstrap: [VisualizerComponent]
})
export class VisualizerModule {}
