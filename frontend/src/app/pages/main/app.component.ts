import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../services';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	public title$: Observable<string>;

	public description$: Observable<string[]>;

	constructor(private service: AppService) {
		this.title$ = this.service.title$;
		this.description$ = this.service.description$;
	}
}
