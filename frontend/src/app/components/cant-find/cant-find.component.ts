import {Component, OnInit} from '@angular/core';
import {AppService} from '../../services';

@Component({
	selector: 'app-cant-find',
	templateUrl: './cant-find.component.html',
	styleUrls: ['./cant-find.component.css']
})
export class CantFindComponent implements OnInit {

	private moduleDescription: string[] = [
		'We don\'t know where you are trying to go...',
		'If you are trying to break the app, the mosquito will bite you >:('
	];

	constructor(private appService: AppService) {
	}

	ngOnInit() {
		setTimeout(() => {
			this.appService.setDescription(this.moduleDescription);
		}, 50);
	}

}
