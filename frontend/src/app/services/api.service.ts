import {Injectable} from '@angular/core';
import {BackendService} from './backend.service';
import {GraphPage} from '../modules/visualizer/interfaces';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	constructor(private backend: BackendService) {
	}

	public uploadResults(form: FormData, basic: boolean): Promise<boolean> {
		const path = basic ? 'upload/basic' : 'upload/genetic';
		return this.backend.postFormData(path, form);
	}

	public getGraphPage(limit: number, offset: number): Promise<GraphPage> {
		return this.backend.get('', {limit, offset});
	}
}
