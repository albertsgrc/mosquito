import { Location } from '@angular/common';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import * as merge from 'deepmerge';

import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root',
})
export class BackendService {
	constructor(private http: HttpClient) {}
	private static createBackendUrl(path: string) {
		return Location.joinWithSlash(environment.baseURL, path);
	}

	public get<T>(path: string, params: {} = {}, headers: any = {}): Promise<T> {
		return this.http.get<T>(BackendService.createBackendUrl(path), {
			params,
			headers: new HttpHeaders(merge(this.headersConfig, headers)),
		}).toPromise();
	}

	public post<T>(
		path: string,
		data: Record<string, any> = {},
		headers: any = {}
	): Promise<T> {
		return this.http.post<T>(BackendService.createBackendUrl(path), JSON.stringify(data), {
			headers: new HttpHeaders(merge(this.headersConfig, headers)),
		}).toPromise();
	}

	public postFormData<T>(path: string, data: FormData, headers: any = {}): Promise<T> {
		return this.http.post<T>(BackendService.createBackendUrl(path), data, {
			headers: new HttpHeaders(this.headersConfigFormData),
		}).toPromise();
	}

	public postFormDataUpload<T>(
		path: string,
		data: FormData,
		options: any = {}
	): Observable<HttpEvent<T>> {
		return this.http.post<T>(BackendService.createBackendUrl(path), data, options);
	}

	public put<T>(
		path: string,
		data: Record<string, any> = {},
		headers: any = {}
	): Promise<T> {
		return this.http.put<T>(BackendService.createBackendUrl(path), JSON.stringify(data), {
			headers: new HttpHeaders(merge(this.headersConfig, headers)),
		}).toPromise();
	}

	public patch<T>(
		path: string,
		data: Record<string, any> = {},
		headers: any = {}
	): Promise<T> {
		return this.http.patch<T>(BackendService.createBackendUrl(path), JSON.stringify(data), {
			headers: new HttpHeaders(merge(this.headersConfig, headers)),
		}).toPromise();
	}

	public delete<T>(path: string, headers: any = {}): Promise<T> {
		return this.http.delete<T>(BackendService.createBackendUrl(path), {
			headers: new HttpHeaders(merge(this.headersConfig, headers)),
		}).toPromise();
	}

	get headersConfig(): any {
		return {
			'Content-Type': 'application/json',
			Accept: 'application/json',
		};
	}

	get headersConfigFormData(): any {
		return {
			Accept: 'application/json',
		};
	}
}
