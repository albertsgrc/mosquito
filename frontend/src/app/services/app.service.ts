import {Injectable} from '@angular/core';
import {Stage} from '../interfaces';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AppService {

	private titleStart = 'MOSQUITO';
	private title: BehaviorSubject<string> = new BehaviorSubject<string>(this.titleStart);

	private descriptionStart: string[] = [
		'Welcome to Mosquito!',
		'Don\'t worry, we only suck data.'
	];
	private description: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(this.descriptionStart);

	private appStageStart: Stage = Stage.BEFORE_UPLOAD;
	private appStage: BehaviorSubject<Stage> = new BehaviorSubject<Stage>(this.appStageStart);

	public title$: Observable<string> = this.title.asObservable();
	public description$: Observable<string[]> = this.description.asObservable();

	public appStage$: Observable<Stage> = this.appStage.asObservable();

	constructor() {}

	public setDescription(descrption: string[]): void {
		this.description.next([...descrption]);
	}
}
