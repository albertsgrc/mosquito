cmake_minimum_required(VERSION 3.13)
project(joc_2018_2019_q1)

set(CMAKE_CXX_STANDARD 20)
#set(CMAKE_CXX_FLAGS "-Wall -Wno-unused-variable -DDEBUG -D_GLIBCXX_DEBUG -g")
set(CMAKE_CXX_FLAGS "-Wall -Wno-unused-variable -Ofast -march=native -g")

include_directories(.)

add_executable(main
        main.cc)