#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

using namespace std;

struct PatientData
{
    vector<float> values;

    PatientData()
    {
        values = vector<float>(19);
    }
};

void usage(char *program)
{
    cout << "Usage: " << string(program) << " mutations_file patient_file output_data_file output_translations_file" << endl;
    exit(1);
}

void add_column(vector<PatientData> &patients_data, vector<string> &column_names, const string &column_name, float value)
{
    column_names.push_back(column_name);

    for (int i = 0; i < patients_data.size(); ++i)
    {
        PatientData &patient_data = patients_data[i];
        patient_data.values.push_back(value);
    }
}

void insert_mutation(const string &mutation, unordered_set<string> &set, int v4, int v5, int v6, vector<PatientData> &patients_data, vector<string> &column_names)
{
    auto it = set.insert(mutation);

    if (it.second)
    {
        add_column(patients_data, column_names, mutation, v6 + v5 * 3 + v4 * 9);
    }
}

void read_mutations_file(char *path, vector<PatientData> &patients_data, unordered_map<string, int> &name_to_index, vector<pair<string, vector<string>>> &translations, vector<string> &column_names, const vector<string> &patient_names)
{
    ifstream file;
    file.open(path);

    string header;
    getline(file, header);
    stringstream ss(header);

    int first_column = column_names.size();
    string column;
    ss >> column; // ignore patient name column
    while (ss >> column)
    {
        column_names.push_back(column);
    }

    string patient;
    unordered_set<string> v3s;

    int first_translation = translations.size();
    translations.push_back({column_names[first_column + 2], {"A", "B"}});

    map<string, map<string, float>> gene_mutation_counts;

    while (file >> patient)
    {
        auto &patient_data = patients_data[name_to_index[patient]];

        string v2, v3, sv4;
        int v5, v6;
        file >> v2 >> v3 >> sv4 >> v5 >> v6;

        int v4 = sv4 == "A" ? 0 : 1;

        gene_mutation_counts[v2][patient]++;

        insert_mutation(v3, v3s, v4, v5, v6, patients_data, column_names);
    }

    for (const auto &pair : gene_mutation_counts)
    {
        column_names.push_back(pair.first);

        for (int i = 0; i < patients_data.size(); ++i)
        {
            PatientData &patient_data = patients_data[i];
            const string& patient_name = patient_names[i];

            auto it = pair.second.find(patient_name);
            patient_data.values.push_back(it == pair.second.end() ? 0 : it->second);
        }
    }

    file.close();
}

int insert_categorical(const string &value, unordered_map<string, int> &set, vector<string> &translations)
{
    auto p = set.insert({value, set.size()});

    if (p.second)
    {
        translations.push_back(value);
    }

    return p.first->second;
}

void read_patients_file(char *path, vector<PatientData> &patients,
                        vector<string> &column_names, vector<pair<string, vector<string>>> &translations,
                        unordered_map<string, int> &name_to_index, vector<string> &patient_names)
{
    ifstream file;
    file.open(path);

    string patient;
    string v7, v8, v9, v12, v14;
    unordered_map<string, int> v7s, v8s, v9s, v12s, v14s;

    string header;
    getline(file, header);
    stringstream ss(header);

    string column, patient_column;
    ss >> patient_column; // ignore patient name
    while (ss >> column)
    {
        column_names.push_back(column);
    }

    translations = {
        {column_names[0], {}},
        {column_names[1], {}},
        {column_names[2], {}},
        {column_names[5], {}},
        {column_names[7], {}},
        {patient_column, {}},
    };

    PatientData patient_data;

    struct comma_separator : std::numpunct<char>
    {
        virtual char do_decimal_point() const override { return ','; }
    };
    file.imbue(std::locale(std::cout.getloc(), new comma_separator));

    while (
        file >> patient >> v7 >> v8 >> v9 >> patient_data.values[3] >> patient_data.values[4] >> v12 >> patient_data.values[6] >> v14 >> patient_data.values[8] >> patient_data.values[9] >> patient_data.values[10] >> patient_data.values[11] >> patient_data.values[12] >> patient_data.values[13])
    {
        name_to_index.insert({patient, patients.size()});

        patient_names.push_back(patient);

        patient_data.values[0] = insert_categorical(v7, v7s, translations[0].second);
        patient_data.values[1] = insert_categorical(v8, v8s, translations[1].second);
        patient_data.values[2] = insert_categorical(v9, v9s, translations[2].second);
        patient_data.values[5] = insert_categorical(v12, v12s, translations[3].second);
        patient_data.values[7] = insert_categorical(v14, v14s, translations[4].second);

        translations[5].second.push_back(patient);

        patients.push_back(patient_data);
    }

    file.close();
}

void print_data(char *path, const vector<string> &column_names, const vector<PatientData> &patients)
{
    ofstream file;
    file.open(path);

    file << column_names[0];
    for (int i = 1; i < column_names.size(); ++i)
    {
        file << ' ' << column_names[i];
    }
    file << endl;

    for (const PatientData &patient : patients)
    {
        file << patient.values[0];
        for (int i = 1; i < patient.values.size(); ++i)
        {
            file << ' ' << patient.values[i];
        }
        file << endl;
    }

    file.close();
}

void print_translations(char *path, const vector<pair<string, vector<string>>> &translations)
{
    ofstream file;
    file.open(path);

    file << "{";

    for (int i = 0; i < translations.size(); ++i)
    {
        const auto &var_translations = translations[i];
        file << '"' << var_translations.first << "\":";
        file << '[';

        for (int j = 0; j < var_translations.second.size(); ++j)
        {
            file << '"' << var_translations.second[j] << '"';

            if (j < var_translations.second.size() - 1)
            {
                file << ',';
            }
        }

        file << ']';
        if (i < translations.size() - 1)
        {
            file << ',';
        }
    }

    file << "}";

    file.close();
}

int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        usage(argv[0]);
    }

    unordered_map<string, int> name_to_index;
    vector<string> column_names;
    vector<PatientData> patients;
    vector<pair<string, vector<string>>> translations;
    vector<string> patient_names;

    read_patients_file(argv[2], patients, column_names, translations, name_to_index, patient_names);
    read_mutations_file(argv[1], patients, name_to_index, translations, column_names, patient_names);

    print_data(argv[3], column_names, patients);
    print_translations(argv[4], translations);
}