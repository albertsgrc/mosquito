all: data-cleaner

data-cleaner: src/data-cleaning/main.cc
	g++ -Ofast -march=native src/data-cleaning/main.cc -o data-cleaner
	
clean:
	rm -rf data-cleaner